#!/bin/bash
# Generates OpenVPN .ccd file

echo -n > openvpn.ccd
while read line
do
	echo "push \"route $line\"" >> openvpn.ccd
done < iplist_collapsed_mask.txt

cp ./openvpn.ccd /etc/openvpn/ccd/DEFAULT
