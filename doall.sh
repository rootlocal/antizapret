#!/bin/sh

# Download list
curl -o list_cp1251.xml https://raw.githubusercontent.com/zapret-info/z-i/master/dump.csv
iconv -f cp1251 -t utf8 list_cp1251.xml > list.xml

# Get IP addresses from list
./getips.sh

# Get hostnames from list
./gethosts.sh

# Resolve A record from hostnames
./resolvehosts.sh

# Collapse IP Addresses
./collapse.py

# Generate .pac with IP addresses
#./genpac-ip.sh

# Generate .pac with IP addresses and hostnames
#./genpac-hostname.sh

# Generate OpenVPN .ccd
#./genopenvpn.sh

# Generate dnsmasq aliases
#./gendnsmasq.sh

# Add IP addresses to iptables
#./setiptables.sh

# Add IP addresses to ipset
# comment setiptables above to use this
./setipset.sh

# Add users who downloaded PAC file to ipset
#./setpacusers.sh

# Copy hosts to /etc/hosts
cat hosts_orig resolved-hostnames-ipdomain.txt > /etc/hosts

# Restart dnsmasq
#service dnsmasq restart
