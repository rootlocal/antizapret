#!/bin/bash

# Add IP addresses to ipset 'zapret' table
echo "Flushing ipset rules"
ipset destroy zapret
ipset flush zapret

# Creating it again
ipset create zapret hash:ip
echo > ipset-save.txt

echo "Reading iplist.txt"
while read line
do
	#iptables -A zapret -d "$line" -j ACCEPT
	echo add zapret "$line" >> ipset-save.txt
done < iplist.txt

# "Restore" iptables with filled zapret table
ipset restore < ipset-save.txt
echo "Done!"

