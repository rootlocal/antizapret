#!/bin/bash
source ./config.sh

# .pac header
echo "// ProstoVPN.AntiZapret PAC-ip File
// Generated on $(date)

function FindProxyForURL(url, host) {
" > proxy.pac

awk -f ipoptim.awk iplist.txt >> proxy.pac

echo "  // Internet Explorer
  if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function(obj, start) {
           for (var i = (start || 0), j = this.length; i < j; i++) {
               if (this[i] === obj) { return i; }
           }
           return -1;
      }
  }
" >> proxy.pac

cp proxy.pac proxy-nossl.pac

echo " var oip = dnsResolve(host);
 var rip = oip.split(\"\");
 var res = -1;
 if (rip[1] == '.')
  res = one.indexOf(oip);

 else if (rip[2] == '.') {
  if (rip[1] <= '3')
   res = two03.indexOf(oip);
  else if (rip[1] <= '6' && rip[1] >= '3')
   res = two46.indexOf(oip);
  else if (rip[1] >= '7')
   res = two79.indexOf(oip);
 }

 else if (rip[2] <= '3')
  res = three03.indexOf(oip);
 else if (rip[2] <= '6' && rip[2] >= '4')
  res = three46.indexOf(oip);
 else if (rip[2] >= '7')
  res = three79.indexOf(oip);

 else
  res = -1;

 if (res != -1) {
    // HTTPS proxy is a HTTP proxy over SSL. It is NOT a CONNECT proxy!
    // Supported only in Chrome and Firefox.
    // http://www.chromium.org/developers/design-documents/secure-web-proxy
    // This is to bypass FULL DPI
" | tee -a proxy.pac proxy-nossl.pac > /dev/null

echo "    return \"PROXY ${PACPROXYHOST}; DIRECT\";" >> proxy-nossl.pac
echo "    return \"HTTPS ${PACHTTPSHOST}; PROXY ${PACPROXYHOST}; DIRECT\";" >> proxy.pac
echo "  }
  return \"DIRECT\";
}" |  tee -a proxy.pac proxy-nossl.pac > /dev/null

cp ./proxy.pac /usr/share/nginx/html/antizapret/
cp ./proxy-nossl.pac /usr/share/nginx/html/antizapret/
rm /usr/share/nginx/html/antizapret/proxy.pac.gz
rm /usr/share/nginx/html/antizapret/proxy-nossl.pac.gz
gzip -k9 /usr/share/nginx/html/antizapret/proxy.pac
gzip -k9 /usr/share/nginx/html/antizapret/proxy-nossl.pac
